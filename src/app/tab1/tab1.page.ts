import { Component } from '@angular/core';
import { TranslationService } from '../api/translation.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  myinput: String = '';
  myoutput: String;
  loadingDialog: any;

  constructor(
    private translationService: TranslationService,
    public loadingController: LoadingController
  ) { }

  public btnTranslateClicked(): void
  {
    if(this.myinput.length >= 2)
    {
      this.presentLoading();
      this.translationService.getTranslation(this.myinput).subscribe( (data) => 
      {
        this.myoutput = data['responseData']['translatedText'];
        this.loadingDialog.dismiss();
      });
    }
  }

  async presentLoading() 
  {
    this.loadingDialog = await this.loadingController.create({
      message: 'Translating ...',
    });
    await this.loadingDialog.present();
  }
}
